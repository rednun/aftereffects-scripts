/*
	Get Age script

	Get current age of a date using current date

	sample usage:

eval( comp("!Functions").layer("Age").text.sourceText.value ); //import function
date_str = comp("!!Settings").layer("birthdate").text.sourceText.value;
age = Age(date_str); //returns integer

*/

function Age(date_str) {
	var input_d = _StringToDate(date_str);
	if(input_d) {
		var now_d = new Date(Date(0));
		var age = now_d.getFullYear() - input_d.getFullYear();
		var m = now_d.getMonth() - input_d.getMonth();
		if(m<0 || (m===0 && now_d.getDate() < input_d.getDate())) {
			age--;
		}	
		return age;
	} else {
		return 0;
	}	
}


function _StringToDate(date_str) {
	var date_arr = [];
	if(date_str.indexOf("/")>0) {
		date_arr = date_str.split("/");
	} else if(date_str.indexOf("-")>0) {
		date_arr = date_str.split("-");
	}
	if(date_arr.length > 2) {
		var day = Number(date_arr[0]);
		var month = Number(date_arr[1])-1;
		var year = Number(date_arr[2]);
		var d = new Date(year,month,day);
		return d;
	} else {
		return 0;
	}
}