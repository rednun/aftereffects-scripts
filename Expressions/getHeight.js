/*
	getHeight.js
	20160229 v0.1 Initial version based on RTL scripts for AE CS5 added getHeightFromTop

	usage:
	eval(comp("!Functions").layer("getHeight").text.sourceText.value); //import Functions

	top_h = getHeightFromTop(this.Layer);
	bot_h = getHeightFromBottom(this.Layer);

*/




function getHeightFromTop(layer) {
	exponent = Math.floor(Math.log(layer.height))-2;
	start = 0;
	for (i=exponent; i>0; i--) {
		precision = Math.pow(2, i); 
		h = sampleTop(layer, precision, start);
		if(h) {
			//set new start position 
			start = h - precision;
		// } else {
		// 	return 0;
		}
	}
	return Math.max(0, layer.transform.yPosition - start);
}


function getHeightFromBottom(layer) {
	exponent = Math.floor(Math.log(layer.height))-2;
	start = 0;
	for (i=exponent; i>0; i--) {
		precision = Math.pow(2, i); 
		h = sampleBottom(layer, precision, start);
		if(h) {
			//set new start position 
			start = h + precision;
		// } else {
		// 	return 0;
		}
	}
	return (start - layer.transform.yPosition);
}

function sampleTop(layer, sampleheight, start) {
	var layerwidth = layer.width;
	var layerheigth = layer.height;
	var posteffect = false;
	var sampletime = 0;
	var ypos = start;
	var radius = [layerwidth/2, sampleheight];
	for (ypos; ypos <= layerheigth; ypos += sampleheight) {
		var point = [layerwidth/2, ypos];
		var rgba = layer.sampleImage(point, radius, posteffect, sampletime);
		var alpha = rgba[3];
		if(alpha>0) {
			//return last position that didnt have pixel data
			// return (ypos - sampleheight);
			return ypos
		}
	};
	//return last used sampleheight if alpha not found
	return false
}



function sampleBottom(layer, sampleheight, start) {
	var layerwidth = layer.width;
	var layerheigth = layer.height;
	var posteffect = false;
	var sampletime = 0;
	var ypos = start > 0 ? start : layerheigth;
	var radius = [layerwidth/2, sampleheight];
	//get first column with pixels from the right
	//use verticalcenter of layer
	for (ypos; ypos >= 0; ypos -= sampleheight) {
		var point = [layerwidth/2, ypos];
		var rgba = layer.sampleImage(point, radius, posteffect, sampletime);
		var alpha = rgba[3];
		if(alpha>0) {
			//return last position that didnt have pixel data
			return (ypos + sampleheight);
		}
	};
	//return last used sampleheight if alpha not found
	return false;
}