/*
	Day Of The Week script

	with offset in days option

	sample usage:

eval( comp("!Functions").layer("DayOfTheWeek").text.sourceText.value ); //import function
date_str = comp("!!Settings").layer("DTE_RDV").text.sourceText.value;
offset_days = thisComp.layer("settings").effect("offset")("Slider");
res = DayOfTheWeek(date_str, offset_days);

*/

function DayOfTheWeek(date_str, offset_days) {
	var days_arr = ["lundi","mardi","mercredi","jeudi","vendredi","samedi","dimanche"];
	var d = _StringToDate(date_str, offset_days);
	var dotw = d.getDay();
	return days_arr[(dotw+6)%7];
}

function MonthName(date_str, offset_days) {
	var months_arr = "janvier février mars avril mai juin juillet août septembre octobre novembre décembre".split(" ");
	var d = _StringToDate(date_str, offset_days);
	var month = d.getMonth();
	return months_arr[month];
}


function DayNumber(date_str, offset_days) {
	var d = _StringToDate(date_str, offset_days);
	return d.getDate();
}


function FullYear(date_str, offset_days) {
	var d = _StringToDate(date_str, offset_days);
	return d.getFullYear();
}


function _StringToDate(date_str, offset_days) {
	var date_arr = [];
	if(date_str.indexOf("/")>0) {
		date_arr = date_str.split("/");
	} else if(date_str.indexOf("-")>0) {
		date_arr = date_str.split("-");
	}
	var d = new Date(2000,1,1); //current date
	if(date_arr.length > 2) {
		var day = Number(date_arr[0]);
		var month = Number(date_arr[1])-1;
		var year = Number(date_arr[2]);
		d = new Date(year,month,day);
	}
	//offset days
	if(offset_days) {
		d.setDate(d.getDate() + offset_days);
	}
	return d;
}