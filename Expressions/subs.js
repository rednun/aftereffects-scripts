'''
// use {field_name} in text to insert variables

var subs = [];
subs.push({"idx":1, "start_f":0, "end_f":97, "text":"Leuk dat u meedoet voor € 10.000,- shoptegoed!"});
subs.push({"idx":1, "start_f":119, "end_f":138, "text":"Zo…"});
subs.push({"idx":2, "start_f":149, "end_f":218, "text":"…nu weten we zeker dat u kans maakt op dat bedrag."});
subs.push({"idx":3, "start_f":247, "end_f":284, "text":"En over kans maken gesproken:"});
subs.push({"idx":4, "start_f":285, "end_f":370, "text":"Bij de BankGiro Loterij geven we elke maand wel…"});
subs.push({"idx":4, "start_f":371, "end_f":411, "text":"… 22x € 10.000,- weg!"});
subs.push({"idx":5, "start_f":440, "end_f":532, "text":"Elke maand 4 auto’s!"});
subs.push({"idx":6, "start_f":533, "end_f":607, "text":"Elke maand 4x € 100.000,-!"});
subs.push({"idx":7, "start_f":608, "end_f":669, "text":"En ook nog eens…"});
subs.push({"idx":7, "start_f":670, "end_f":739, "text":"… € 1 MILJOEN!"});
subs.push({"idx":7, "start_f":749, "end_f":807, "text":"Elke maand opnieuw!"});
subs.push({"idx":8, "start_f":808, "end_f":883, "text":"U heeft het gezien: een record aan geldprijzen."});
subs.push({"idx":9, "start_f":884, "end_f":966, "text":"En als u nú gaat meespelen in de BankGiro Loterij…"});
subs.push({"idx":9, "start_f":967, "end_f":1048, "text":"… dan doe ik er ook nog een VVV-bon bij."});
subs.push({"idx":11, "start_f":1049, "end_f":1084, "text":"Weet u wat…?"});
subs.push({"idx":12, "start_f":1085, "end_f":1199, "text":"U krijgt voor € 20,- aan VVV-bonnen!"});
subs.push({"idx":13, "start_f":1240, "end_f":1299, "text":"Als ú hieronder nog wat gegevens invult…"});
subs.push({"idx":13, "start_f":1300, "end_f":1359, "text":"… dan stuur ik alvast de VVV-bonnen naar u op."});
subs.push({"idx":14, "start_f":1378, "end_f":1442, "text":"Ik zou het wel weten als ik u was!"});

function show_sub(subs, seconds) {
	var cf = Math.round((seconds || time)*25);
	//go through all subs
	for (var i = subs.length - 1; i >= 0; i--) {
		sub_obj = subs[i];
		if(sub_obj.start_f <= cf && cf <= sub_obj.end_f) {
			t = sub_obj.text;
			t = personalize(t);
			t = t.replace(/\\r/g, "\\r");
			return t;
		}
	};
	return ""; //cf; //return empty if not found
}

function personalize(str) {
	matches = str.match(/\\{.+\\}/g);
	if(matches) {
		for (var i = matches.length - 1; i >= 0; i--) {
			match = matches[i];
			lookup = match.substring(1, match.length-1);
			//val = comp("!!Settings").layer(lookup).text.sourceText.value;
			//replace with spaces for RT flow
			val = "     ";
			str = str.replace(match, val);
		};
	}
	return str;
}

show_sub(subs);

//https://jsfiddle.net/Rednun/su88aycj/13/
//$.evalFile("~/Dropbox/_MobileProjects/16001_BGL_Campagne_B318_Spotta-RT/40 Video Template/41 AFX Werkmap/PVM/AE Project/subs.js");
'''