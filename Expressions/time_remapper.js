/*
	time_remapper for Adobe After Effects
	(c) guido@rednun.nl

	version 1.0 150827 - Built for RTL7 and RTL8
	version 1.1 151002 - Capped output frame to source out frame
	version 1.2 151028 - Default blank_endpoints disabled

	retime parts of a comp based on a list of timings of the original comp (src)
	and a list of wanted timings for each part (tgt)
	both lists should be equal in length

	if in or out in tgt_list is negative, its counterpart will be derived from the duration of the src part
	e.g. with src = [0,20] ; tgt = [10,-1] is the same as tgt = [10,30]
*/

/* Sample Usage #1
var scr_list = [[0,20], [30,50]];
var tgt_list = [[0,-1], [-1,100]];
time_remapper(scr_list, tgt_list);
*/

/* Sample Usage #2 with external effect sliders
//nb: these are not points but two values representing source in and out point or target in and out point!
var scr_list = [];
scr_list.push(effect("src_a")("Point"));
scr_list.push(effect("src_b")("Point"));
scr_list.push(effect("src_c")("Point"));
var tgt_list = [];
tgt_list.push(effect("tgt_a")("Point"));
tgt_list.push(effect("tgt_b")("Point"));
tgt_list.push(effect("tgt_c")("Point"));
time_remapper(scr_list, tgt_list);
*/


function time_remapper(src_list, tgt_list, blank_endpoints) {
	var default_blank_endpoints = false;
	/*
		retime parts of a comp based on a list of timings of the original comp (src)
		and a list of wanted timings for each part (tgt)
		both lists should be equal in length

		input
			 src_list: list of in/out points array of source animation
			 tgt_list: list of wanted in/out points array of parts
		output
			retimed framenumbers
		example
			time_remapper([[0,20], [30, 50]], [[0,-1], [-1, 100]])
	*/
	var time_f = time*25; //convert seconds to frames
	var result_f = -1; //(-1) default return value; -1 means hidden
	blank_endpoints = (typeof blank_endpoints == 'undefined' ? default_blank_endpoints : blank_endpoints); //[0/1] (1) if set return -1 where the layer should be hidden


	//run through each part
	for (var i = 0; i < src_list.length; i++) {
		var src_arr = src_list[i];
		var tgt_arr = tgt_list[i];
		var src_dur_f = src_arr[1] - src_arr[0];
		tgt_in_f = tgt_arr[0] < 0 ? tgt_arr[1]-src_dur_f : tgt_arr[0];
		tgt_out_f = tgt_arr[1] < 0 ? tgt_arr[0]+src_dur_f : tgt_arr[1];
		if(time_f < tgt_in_f) {
			//before beginning
			if(i==0 && blank_endpoints) {
				result_f = -1;
			}
		} else if (tgt_in_f <= time_f && time_f <= tgt_out_f) {
			//within tgt animation
			result_f = src_arr[0] + (time_f - tgt_in_f); //speed is always 100%
			result_f = Math.min(src_arr[1], result_f); //do not allow value higher then outpoint of source
			break; //do not continue after we have a hit
		} else if (tgt_out_f < time_f) {
			//after tgt animation
			if(i==src_list.length-1 && blank_endpoints) {
				result_f = -1;
			} else {
				result_f = src_arr[1];
			}
		}
	};

	return result_f;
}