/*
	Booleanize.js
	151027	Safe converting of a string to a Boolean; result is false on error/exception
	160803	added type detection and negative values for errors

	usage:
	eval(comp("!Functions").layer("Booleanize").text.sourceText.value); //import Booleanize()
	var str = "1";
	var bool = Booleanize(str); //true
*/

function Booleanize(str) {
	var bool = false;
	if(typeof str == "object") {
		try {
			//assume checkbox
			str = str.value;
		} catch (err) {
			return false;
		}
	}
	if(typeof str == "number") {
		str = str.toString();
	}
	try {
		//if(str == "") throw "empty";
		//if(str.substr(0,1) == "{") throw "unfilled";
		if(str == "") return false;
		if(str.substr(0,1) == "{") return false;
		if(str == "0") return false;
		if(str.toLowerCase() == "false") return false;
		if(str == "1" || str.toLowerCase() == "true" || str.toLowerCase() == "on") return true;
	}
	catch(err) {
		// return "[Error parsing ["+str+"] -["+err+"]";
		// return false;
		return false;
	}
	return bool;
}