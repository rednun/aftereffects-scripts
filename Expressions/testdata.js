/* dummy data to put in parameters of textlayers in !!Settings comp
	usage:
default_value = "";
eval( comp("!Functions").layer("testdata").text.sourceText.value );
*/


//below the code to evaluate
var testdata_idx = Number(thisComp.layer("settings").text.sourceText.value);
//get dummy/test data
var testdata_arr = [default_value, this.name, value];
//add csv data to array
var csv = comp("!DummyData").layer("csv_data").text.sourceText.value;
var list = CsvToArray(csv, this.name);
testdata_arr = testdata_arr.concat(list);
//decide which data to show
res = value;
if(res.substr(0,1) == "{") {
	testdata_idx = Math.min(testdata_idx, testdata_arr.length-1);
	res = testdata_idx > 0 ? testdata_arr[testdata_idx] : default_value;
	res = testdata_idx > 0 && (res == "" || res.substr(0,1) == "{" ? default_value : res;
}
res;

// -- functions --
function CsvToArray(csvdata, field) {
	var delimeter = ";";
	//get header
	var csv_arr = csvdata.match(/[^\r\n]+/g);
	var header_arr = csv_arr.shift().split(delimeter);
	var data_obj = new Object();
	data_arr = new Array();
	//add row data to data object
	for(var i=0;i<csv_arr.length;i++) {
		var row_data = csv_arr[i].split(delimeter);
		for(var j=0;j<row_data.length;j++) {
			var field_name = header_arr[j];
			if(field_name == field) {
				var obj_value = row_data[j];
				data_arr.push(obj_value);
			}
		}
	}
	//return data object
	return data_arr;
}