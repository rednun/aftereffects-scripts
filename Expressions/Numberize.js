/*
	Numberize.js
	20151027 v0.1 Safe converting of a string to a number; result is 0 on error/exception
	20151125 v0.2 Improved handling of non numbers
	20160916 Handle , decimals in strings

	usage:
	eval(comp("!Functions").layer("Numberize").text.sourceText.value); //import Numberize()
	var str = "1";
	var nr = Numberize(str); //1
*/

function Numberize(str) {
	var n = 0;
	try { 
		if(str == "") throw "empty";
		//replace , with .
		str = str.replace(",", ".");
		n = Number(str);
		if(isNaN(n)) throw "not a number";
	}
	catch(err) {
		//throw "[Error parsing ["+str+"] -["+err+"]";
		n = 0;
	}
	return n;
}


