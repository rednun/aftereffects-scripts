scaleMe(0);

function scaleMe(fit) {
	//returns scale (two dimensions)
	//use fit=0 for fill, fit=1 for scale-to-fit
	fit = fit == "1" || fit == "true" || fit == "on" ? true : false;
	sw = thisComp.width/width;
	sy = thisComp.height/height;
	s = fit ? Math.min(sw, sy) : Math.max(sw, sy);
	return [s,s]*100;
}