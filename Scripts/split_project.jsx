﻿{
	// split_project_{version}.jsx
	// guido@rednun.nl
	// 
	// version
	// 160607 Initial version
	// 170411 Forced saving to .aepx 
	//
	// Reduce project for every single selected comp (and comps starting with `!`)
	// Saving to projectname with the comp name added in same folder


	function SplitProject(thisObj)
	{
		var scriptName = "Split Project";
		var KeepCompPrefix = "!"; //dont reduce these comps
		var SplitComps = []; //selected comps to split project into
		var master_project = app.project.file;

		//check if project is saved, else present dialog to save
		app.project.save();

		//check if we have selection of comps
		var comps_selected = getSelectedCompNames(KeepCompPrefix); //array of strings
		// Debug(comps_selected);

		// nb. if the selection changes, so does the array holding the CompItem objects,
		// so we store only the names and look them up again

		// var comps_fixed = searchCompNames(KeepCompPrefix); //array of strings
		// Debug(comps_fixed);


		//for every selected comp
		if(comps_selected) {
			var cnt = comps_selected.length;
			for (var i=0; i<cnt; i++) {
				//search comp by name
				var found_comp_array = searchComps(comps_selected[i], true); //array of compitems
				// Debug(found_comp_array);
				var selected_comp = found_comp_array[0]; //CompItem object
			
				//save as new project with comp
				saveVersion(selected_comp.name);
			
				//select right comps
				//find fixed ! comps
				var comps_list = searchComps(KeepCompPrefix); //returns array of CompItem objects
				//add selected comp
				comps_list.push(selected_comp);

				//Debug(comps_list);

				//reduce project
				app.project.reduceProject(comps_list);

				//save
				app.project.save();

				//open original project
				app.open(master_project);

				//repeat for next comp in list
			}
		}
	}


	function saveVersion(version)
	{
		// app.project.saveWithDialog();
		var file_name = app.project.file.toString();
		// var file_ext = file_name.substr(file_name.lastIndexOf("."));
		var file_ext = '.aepx';
		var file_name_new = file_name.substr(0, file_name.lastIndexOf(".")) + "_" + version + file_ext;
		// alert(file_name_new);
		var file_obj = new File(file_name_new);
		app.project.save(file_obj);
	}


	function SaveProject()
	{
		// app.project.saveWithDialog();
		app.project.save();
	}


	function getSelectedCompNames(exclude_prefix)
	{
		var sel = app.project.selection;
		var clean_list = [];
		if(sel.length > 0) {
			//check selection for comps
			for (var i = sel.length - 1; i >= 0; i--) {
				if(sel[i].toString() == "[object CompItem]") {
					if(sel[i].name.substr(0, exclude_prefix.length) != exclude_prefix) {
						clean_list.push(sel[i].name);
					}
				}
			}
			//return
			if(clean_list.length > 0) {
				return clean_list;
			} else {
				alert("Select one or more comps to keep");
			}
		} else {
			alert("Select one or more comps to keep");
		}
	}

	function searchComps(prefix, exactmatch)
	{
		//search and return array of CompItem objects
		var project_items = app.project.items;
		// alert(project_items.toString());
		// Debug(project_items);

		var return_list = [];
		var offset = 0;
		if(project_items.toString() == "[object ItemCollection]") offset=1;
		for (var i=offset; i <= project_items.length; i++) {
			try { //catch lists that don't start at 0
				if(project_items[i].toString() == "[object CompItem]") {
					if(exactmatch) {
						if(project_items[i].name == prefix) {
							return_list.push(project_items[i]);
						}
					} else if(project_items[i].name.substr(0, prefix.length) == prefix) {
						return_list.push(project_items[i]);
					}
				}
			} catch (e) {}
		}
		return return_list;
	}


	function searchCompNames(prefix, exactmatch)
	{
		//search and return array of strings with comp names
		var return_list = [];
		var search = searchComps(prefix, exactmatch); //array of CompItem objects
		for (var i = search.length - 1; i >= 0; i--) {
			return_list.push(search[i].name);
		}
		return return_list;
	}

	function Debug(obj) {
		alert("Dumper\n" + Dumper(obj));
	}

	function Dumper(obj,max,level) {
		var dumped_text = "";
		if(!max) max = 0;
		if(!level) level = 0;
		if(!obj) return;
		var level_padding = "";
		for(var j=0;j<level+1;j++) level_padding += "    ";
		if(typeof(obj) == 'object') {
			if (typeof(obj.length)=="number") {
				//array
				dumped_text += "array\n";
				for (var i = obj.length - 1; i >= 0; i--) {
					try { //sometimes an array starts with 1 instead of 0 in AFX, like items in a project
						var item = obj[i].name;
						var value = obj[i];
						if(typeof(value) == "object") {
							dumped_text += "object\n";
							dumped_text += level_padding + item + " ... " + value.toString() + "\n";
							dumped_text += Dumper(value,max,level+1);
						} else {
							dumped_text += level_padding + "[" + i + "] " + value + "\n";						
						}
					} catch(e) {};
				}
			} else {
				//object
				for(var item in obj) {
					if(level<max) {
						dumped_text += "object\n";
						dumped_text += Dumper(item,max,level+1) + "\n";
					} else {
						//dumped_text += item.name + "\n";
					}
				}
			}
		} else { 
			dumped_text = ""+obj+" ("+typeof(obj)+"), ";
		}
		
		return dumped_text;
	}
	
	
	SplitProject(this);
}
