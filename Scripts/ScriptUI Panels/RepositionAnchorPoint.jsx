/*
 RepositionAnchorPoint.jsx
 version: 3.6
 author: Charles Bordenave (www.nabscripts.com)
 date: 31 Dec 2013
*/


function RepositionAnchorPoint()
{
	var repositionAnchorPoint = this;

	var utils = new RepositionAnchorPointUtils();

	// infos
	this.scriptName = "RepositionAnchorPoint.jsx";
	this.scriptVersion = "3.6";
	this.scriptTitle = "Reposition Anchor Point";
	this.scriptCopyright = "Copyright (c) 2012 Charles Bordenave";
	this.scriptHomepage = "http://www.nabscripts.com";
	this.scriptDescription = {en:"This script allows you to reposition the anchor point of the selected layers around the layer edges while keeping the layers at the same position in the comp window.",
							  fr:"Ce script permet de repositionner le point d\\'ancrage des calques sélectionnés sur le contour du calque tout en maintenant les calques à leur place dans la fenêtre de composition."};
	this.scriptAbout = {en:this.scriptName + ", v" + this.scriptVersion + "\\r" + this.scriptCopyright + "\\r" + this.scriptHomepage + "\\r\\r" + utils.loc(this.scriptDescription),
						fr:this.scriptName + ", v" + this.scriptVersion + "\\r" + this.scriptCopyright + "\\r" + this.scriptHomepage + "\\r\\r" + utils.loc(this.scriptDescription)};
	this.scriptUsage = {en:	"\u25BA Select at least one layer \\r" +
							"\u25BA Choose new anchor location \\r" +
							"\u25BA Click on Reposition to actually reposition the anchor point of all selected layers",
						fr:	"\u25BA Sélectionnez au moins un calque \\r" +
							"\u25BA Choisissez la position du point d\\\'ancrage \\r" +
							"\u25BA Cliquez sur Repositionner pour effectivement repositionner le point d\\\'ancrage des calques sélectionnés"};

	// errors
	this.resErr = {en:"An error occured when trying to open and read a resource file. Make sure '(" + this.scriptName.substring(0, this.scriptName.lastIndexOf(".")) + "_Resources)' folder is located next to the script file.", fr:"Une erreur est survenue lors de l'ouverture d'un fichier de ressource. Vérifiez que le dossier '(" + this.scriptName.substring(0, this.scriptName.lastIndexOf(".")) + "_Resources)' est situé dans le même dossier que le script."};
	this.noCompErr = {en:"A comp must be active.", fr:"Une composition doit être active."};
	this.noLayersErr = {en:"Select at least one layer.", fr:"Sélectionnez au moins un calque."};
	this.processErr = {en:"An error occurred while manipulating layers.", fr:"Une erreur s'est produite pendant la manipulation des calques."};

	// UI strings
	this.aboutBtnName = "?";
	this.anchorStName = {en:"Anchor:", fr:"Ancrage:"};
	this.runBtnName = {en:"Reposition", fr:"Repositionner"};

	// internals
	this.scriptPath = File($.fileName).parent.fsName;
	this.resPath = this.scriptPath + "/(" + this.scriptName.substring(0, this.scriptName.lastIndexOf(".")) + "_Resources)";


	this.buildUI = function (thisObj)
	{
		// dockable panel or palette
		var pal = (thisObj instanceof Panel) ? thisObj : new Window("palette", this.scriptTitle, undefined, {resizeable:false});

		// resource specifications
		var res =
		"group { orientation:'column', alignment:['left','top'], alignChildren:['right','top'], \
			gr1: Group { \
				aboutBtn: Button { text:'" + this.aboutBtnName + "', preferredSize:[25,20] } \
			}, \
			gr2: Group { orientation:'row', alignment:['fill','fill'], \
				gr21: Group { \
					anchorSt: StaticText { text:'" + utils.loc(this.anchorStName) + "' } \
				}, \
				gr22: Group { orientation:'column', spacing:2, \
					gr221: Group { orientation:'row', spacing:3 }, \
					gr222: Group { orientation:'row', spacing:3 }, \
					gr223: Group { orientation:'row', spacing:3 } \
				} \
			}, \
			gr3: Group { orientation:'row', alignment:['fill','top'], \
				runBtn: Button { text:'" + utils.loc(this.runBtnName) + "', alignment:['right','center'] } \
			} \
		}";
		pal.gr = pal.add(res);

		var topLeftImageObj, topCenterImageObj, topRightImageObj;
		var leftCenterImageObj, centerImageObj, rightCenterImageObj;
		var bottomLeftImageObj, bottomCenterImageObj, bottomRightImageObj;

		var topLeftActiveImageObj, topCenterActiveImageObj, topRightActiveImageObj;
		var leftCenterActiveImageObj, centerActiveImageObj, rightCenterActiveImageObj;
		var bottomLeftActiveImageObj, bottomCenterActiveImageObj, bottomRightActiveImageObj;

		try
		{
			// iconic image states: normal, disabled, pressed, rollover
			topLeftImageObj = ScriptUI.newImage( this.resPath + "/topleft.png", undefined, undefined, this.resPath + "/topleft.png" );
			topCenterImageObj = ScriptUI.newImage( this.resPath + "/topcenter.png", undefined, undefined, this.resPath + "/topcenter.png" );
			topRightImageObj = ScriptUI.newImage( this.resPath + "/topright.png", undefined, undefined, this.resPath + "/topright.png" );
			leftCenterImageObj = ScriptUI.newImage( this.resPath + "/leftcenter.png", undefined, undefined, this.resPath + "/leftcenter.png" );
			centerImageObj = ScriptUI.newImage( this.resPath + "/center.png", undefined, undefined, this.resPath + "/center.png" );
			rightCenterImageObj = ScriptUI.newImage( this.resPath + "/rightcenter.png", undefined, undefined, this.resPath + "/rightcenter.png" );
			bottomLeftImageObj = ScriptUI.newImage( this.resPath + "/bottomleft.png", undefined, undefined, this.resPath + "/bottomleft.png" );
			bottomCenterImageObj = ScriptUI.newImage( this.resPath + "/bottomcenter.png", undefined, undefined, this.resPath + "/bottomcenter.png" );
			bottomRightImageObj = ScriptUI.newImage( this.resPath + "/bottomright.png", undefined, undefined, this.resPath + "/bottomright.png" );

			topLeftActiveImageObj = ScriptUI.newImage( this.resPath + "/topleft_active.png", undefined, undefined, this.resPath + "/topleft_active.png" );
			topCenterActiveImageObj = ScriptUI.newImage( this.resPath + "/topcenter_active.png", undefined, undefined, this.resPath + "/topcenter_active.png" );
			topRightActiveImageObj = ScriptUI.newImage( this.resPath + "/topright_active.png", undefined, undefined, this.resPath + "/topright_active.png" );
			leftCenterActiveImageObj = ScriptUI.newImage( this.resPath + "/leftcenter_active.png", undefined, undefined, this.resPath + "/leftcenter_active.png" );
			centerActiveImageObj = ScriptUI.newImage( this.resPath + "/center_active.png", undefined, undefined, this.resPath + "/center_active.png" );
			rightCenterActiveImageObj = ScriptUI.newImage( this.resPath + "/rightcenter_active.png", undefined, undefined, this.resPath + "/rightcenter_active.png" );
			bottomLeftActiveImageObj = ScriptUI.newImage( this.resPath + "/bottomleft_active.png", undefined, undefined, this.resPath + "/bottomleft_active.png" );
			bottomCenterActiveImageObj = ScriptUI.newImage( this.resPath + "/bottomcenter_active.png", undefined, undefined, this.resPath + "/bottomcenter_active.png" );
			bottomRightActiveImageObj = ScriptUI.newImage( this.resPath + "/bottomright_active.png", undefined, undefined, this.resPath + "/bottomright_active.png" );
		}
		catch(e)
		{
			var err = repositionAnchorPoint.resErr;
			utils.throwErr(err);
			return;
		}

		var imageObjs = [topLeftImageObj,topCenterImageObj,topRightImageObj,leftCenterImageObj,centerImageObj,rightCenterImageObj,bottomLeftImageObj,bottomCenterImageObj,bottomRightImageObj];
		var imageActiveObjs = [topLeftActiveImageObj,topCenterActiveImageObj,topRightActiveImageObj,leftCenterActiveImageObj,centerActiveImageObj,rightCenterActiveImageObj,bottomLeftActiveImageObj,bottomCenterActiveImageObj,bottomRightActiveImageObj];

		var btn1 = pal.gr.gr2.gr22.gr221.add( "iconbutton", [0,0,25,23], imageObjs[0], { style:"toolbutton" } );
		var btn2 = pal.gr.gr2.gr22.gr221.add( "iconbutton", [0,0,25,23], imageObjs[1], { style:"toolbutton" } );
		var btn3 = pal.gr.gr2.gr22.gr221.add( "iconbutton", [0,0,25,23], imageObjs[2], { style:"toolbutton" } );

		var btn4 = pal.gr.gr2.gr22.gr222.add( "iconbutton", [0,0,25,23], imageObjs[3], { style:"toolbutton" } );
		var btn5 = pal.gr.gr2.gr22.gr222.add( "iconbutton", [0,0,25,23], imageObjs[4], { style:"toolbutton" } );
		var btn6 = pal.gr.gr2.gr22.gr222.add( "iconbutton", [0,0,25,23], imageObjs[5], { style:"toolbutton" } );

		var btn7 = pal.gr.gr2.gr22.gr223.add( "iconbutton", [0,0,25,23], imageObjs[6], { style:"toolbutton" } );
		var btn8 = pal.gr.gr2.gr22.gr223.add( "iconbutton", [0,0,25,23], imageObjs[7], { style:"toolbutton" } );
		var btn9 = pal.gr.gr2.gr22.gr223.add( "iconbutton", [0,0,25,23], imageObjs[8], { style:"toolbutton" } );

		// default selection
		btn5.value = true; // center
		btn5.image = imageActiveObjs[4];

		var btns = [btn1,btn2,btn3,btn4,btn5,btn6,btn7,btn8,btn9];

		btn1.onClick = function ()
		{
			this.value = true;
			this.image = imageActiveObjs[0];
			for (var i = 0; i < btns.length; i++)  if (i != 0)
			{
				btns[i].image = imageObjs[i];
				btns[i].value = false;
			}
		};

		btn2.onClick = function ()
		{
			this.value = true;
			this.image = imageActiveObjs[1];
			for (var i = 0; i < btns.length; i++) if (i != 1)
			{
				btns[i].image = imageObjs[i];
				btns[i].value = false;
			}
		};

		btn3.onClick = function ()
		{
			this.value = true;
			this.image = imageActiveObjs[2];
			for (var i = 0; i < btns.length; i++) if (i != 2)
			{
				btns[i].image = imageObjs[i];
				btns[i].value = false;
			}
		};

		btn4.onClick = function ()
		{
			this.value = true;
			this.image = imageActiveObjs[3];
			for (var i = 0; i < btns.length; i++) if (i != 3)
			{
				btns[i].image = imageObjs[i];
				btns[i].value = false;
			}
		};

		btn5.onClick = function ()
		{
			this.value = true;
			this.image = imageActiveObjs[4];
			for (var i = 0; i < btns.length; i++) if (i != 4)
			{
				btns[i].image = imageObjs[i];
				btns[i].value = false;
			}
		};

		btn6.onClick = function ()
		{
			this.value = true;
			this.image = imageActiveObjs[5];
			for (var i = 0; i < btns.length; i++) if (i != 5)
			{
				btns[i].image = imageObjs[i];
				btns[i].value = false;
			}
		};

		btn7.onClick = function ()
		{
			this.value = true;
			this.image = imageActiveObjs[6];
			for (var i = 0; i < btns.length; i++) if (i != 6)
			{
				btns[i].image = imageObjs[i];
				btns[i].value = false;
			}
		};

		btn8.onClick = function ()
		{
			this.value = true;
			this.image = imageActiveObjs[7];
			for (var i = 0; i < btns.length; i++) if (i != 7)
			{
				btns[i].image = imageObjs[i];
				btns[i].value = false;
			}
		};

		btn9.onClick = function ()
		{
			this.value = true;
			this.image = imageActiveObjs[8];
			for (var i = 0; i < btns.length; i++) if (i != 8)
			{
				btns[i].image = imageObjs[i];
				btns[i].value = false;
			}
		};

		// event callbacks
		pal.gr.gr1.aboutBtn.onClick = function ()
		{
			utils.createAboutDlg(repositionAnchorPoint.scriptAbout, repositionAnchorPoint.scriptUsage);
		};

		pal.gr.gr3.runBtn.onClick = function ()
		{
			repositionAnchorPoint.reposition(pal);
		};

		// show user interface
		if (pal instanceof Window)
		{
			pal.center();
			pal.show();
		}
		else
		{
			pal.layout.layout(true);
		}
	};

	// Determines whether the active item is a composition
	this.checkActiveItem = function ()
	{
		return !(app.project.activeItem instanceof CompItem);
	};

	// Repositions the anchor point of a set of layers
	this.repositionLayers = function (newLocId, layers)
	{
		var comp = layers[0].containingComp;
		var curTime = comp.time;

		for (var k = 0; k < layers.length; k++)
		{
			var layer = layers[k];
			var bBox = utils.getLayerBoundingBox(layer);

			if (bBox)
			{
				var anchPt = layer.anchorPoint;
				var pos = layer.position;

				var halfW = bBox.width / 2;
				var halfH = bBox.height / 2;
				var xoffset = bBox.xoffset;
				var yoffset = bBox.yoffset;

				var iax = (newLocId % 3) - 1;
				var iay = Math.floor(newLocId / 3) - 1;

				var ipx = newLocId % 3;
				var ipy = Math.floor(newLocId / 3);

				// adjustment for text layer
				// for standard layer, (0,0) is always the top left corner of the layer
				// for text layer, (0,0) is on the bottom egde of the bounding box, on the left/center/right according to the text justication

				if (layer instanceof TextLayer && layer.Masks.numProperties < 1)
				{
					// text justification
					var textDocVal = layer.property("ADBE Text Properties").property("ADBE Text Document").value;

					if (textDocVal.justification == ParagraphJustification.LEFT_JUSTIFY)
					{
						iay -= 2; // -textH
						ipy -= 2; // -textH
						//xoffset += 0;
					}
					else if (textDocVal.justification == ParagraphJustification.CENTER_JUSTIFY)
					{
						iax -= 1; // -textW/2
						iay -= 2; // -textH
						ipx -= 1; // -textW/2
						ipy -= 2; // -textH
						xoffset += halfW;
					}
					else if (textDocVal.justification == ParagraphJustification.RIGHT_JUSTIFY)
					{
						iax -= 2; // -textW
						iay -= 2; // -textH
						ipx -= 2; // -textW
						ipy -= 2; // -textH
						xoffset += 2 * halfW;
					}
				}

				// If rotation or orientation is driven by expression, we convert it to static value or keyframe
				if (layer.threeDLayer)
				{
					if (layer.orientation.expressionEnabled) { layer.orientation.numKeys ? layer.orientation.setValueAtTime(curTime, layer.orientation.valueAtTime(curTime,false)) : layer.orientation.setValue(layer.orientation.valueAtTime(curTime,false)); layer.orientation.expression = ""; }
					if (layer.rotationX.expressionEnabled) { layer.rotationX.numKeys ? layer.rotationX.setValueAtTime(curTime, layer.rotationX.valueAtTime(curTime,false)) : layer.rotationX.setValue(layer.rotationX.valueAtTime(curTime,false)); layer.rotationX.expression = ""; }
					if (layer.rotationY.expressionEnabled) { layer.rotationY.numKeys ? layer.rotationY.setValueAtTime(curTime, layer.rotationY.valueAtTime(curTime,false)) : layer.rotationY.setValue(layer.rotationY.valueAtTime(curTime,false)); layer.rotationY.expression = ""; }
					if (layer.rotationZ.expressionEnabled) { layer.rotationZ.numKeys ? layer.rotationZ.setValueAtTime(curTime, layer.rotationZ.valueAtTime(curTime,false)) : layer.rotationZ.setValue(layer.rotationZ.valueAtTime(curTime,false)); layer.rotationZ.expression = ""; }
				}
				else
				{
					if (layer.rotation.expressionEnabled) { layer.rotation.numKeys ? layer.rotation.setValueAtTime(curTime, layer.rotation.valueAtTime(curTime,false)) : layer.rotation.setValue(layer.rotation.valueAtTime(curTime,false)); layer.rotation.expression = ""; }
				}

				// If property is driven by expression, we convert the expression to static value or keyframe
				if (anchPt.expressionEnabled) anchPt.numKeys ? anchPt.setValueAtTime(curTime, anchPt.valueAtTime(curTime,false)) : anchPt.setValue(anchPt.valueAtTime(curTime,false));
				if (pos.expressionEnabled) pos.numKeys ? pos.setValueAtTime(curTime, pos.valueAtTime(curTime,false)) : pos.setValue(pos.valueAtTime(curTime,false));


				// Apply expressions
				anchPt.expression =
				"fromWorld(toWorld([" + halfW + "," + halfH + ",0] + [" + iax + "*" + halfW + "+" + xoffset + "," + iay + "*"+ halfH + "+" + yoffset + ",0]));";

				pos.expression =
				"try {\r" +  				"	parent.fromWorld(toWorld([" + halfW + "," + halfH + ",0] + [" + iax + "*" + halfW + "+" + xoffset + "," + iay + "*"+ halfH + "+" + yoffset + ",0]));\r" +				"}\r" +				"catch(e)\r" +				"{\r" +				"  toWorld([" + halfW + "," + halfH + ",0] + [" + iax + "*" + halfW + "+" + xoffset + "," + iay + "*"+ halfH + "+" + yoffset + ",0]);\r" +				"}";
				//"toWorld([" + ipx + "*" + halfW + "+" + xoffset + "," + ipy + "*" + halfH + "+" + yoffset + ",0]);";

				// Little refreshing trick
				pos.expressionEnabled = false;
				pos.expressionEnabled = true;

				var anchPtVal = anchPt.valueAtTime(curTime, false);
				anchPt.expression = "";

				var posVal = pos.valueAtTime(curTime, false);
				pos.expression = "";

				// Set value
				anchPt.numKeys ? anchPt.setValueAtTime(curTime, anchPtVal) : anchPt.setValue(anchPtVal);
				pos.numKeys ? pos.setValueAtTime(curTime, posVal) : pos.setValue(posVal);
			}
			//layer.selected = true;
		}
	};

	// Functional part of the script: repositions the anchor point of the selected layers
	this.reposition = function (pal)
	{
		try
		{
			var comp = app.project.activeItem;
			var err = this.noCompErr;
			if (this.checkActiveItem(comp)) throw(err);

			var selLayers = comp.selectedLayers;
			var err = this.noLayersErr;
			if (selLayers.length < 1) throw(err);

			var newLocId;
			var btns = [pal.gr.gr2.gr22.gr221.children[0], pal.gr.gr2.gr22.gr221.children[1], pal.gr.gr2.gr22.gr221.children[2],
						pal.gr.gr2.gr22.gr222.children[0], pal.gr.gr2.gr22.gr222.children[1], pal.gr.gr2.gr22.gr222.children[2],
						pal.gr.gr2.gr22.gr223.children[0], pal.gr.gr2.gr22.gr223.children[1], pal.gr.gr2.gr22.gr223.children[2]];

			for (var i = 0; i < btns.length; i++)
			if (btns[i].value)
			{
				newLocId = i;
				break;
			}

			app.beginUndoGroup(this.scriptTitle);

			this.repositionLayers(newLocId, selLayers);

			app.endUndoGroup();
		}
		catch(err)
		{
			utils.throwErr(err);
		}
	};

	this.run = function (thisObj)
	{
		this.buildUI(thisObj);
	};
}


// utilities
function RepositionAnchorPointUtils()
{
	var utils = this;

	this.loc = function (str)
	{
		var lang = parseFloat(app.version) < 9 ? $.locale : app.isoLanguage;
		return lang.toLowerCase().match("fr") ? str.fr : str.en;
	};

	this.throwErr = function (err)
	{
		var title = $.fileName.substring($.fileName.lastIndexOf("/")+1, $.fileName.lastIndexOf("."));
		alert(this.loc(err), title, true);
	};

	this.createAboutDlg = function (aboutStr, usageStr)
	{
		eval(unescape('%09%09%76%61%72%20%64%6C%67%20%3D%20%6E%65%77%20%57%69%6E%64%6F%77%28%22%64%69%61%6C%6F%67%22%2C%20%22%41%62%6F%75%74%22%29%3B%0A%09%20%20%20%20%20%20%09%20%20%20%20%20%20%20%09%0A%09%20%20%20%20%76%61%72%20%72%65%73%20%3D%0A%09%09%22%67%72%6F%75%70%20%7B%20%6F%72%69%65%6E%74%61%74%69%6F%6E%3A%27%63%6F%6C%75%6D%6E%27%2C%20%61%6C%69%67%6E%6D%65%6E%74%3A%5B%27%66%69%6C%6C%27%2C%27%66%69%6C%6C%27%5D%2C%20%61%6C%69%67%6E%43%68%69%6C%64%72%65%6E%3A%5B%27%66%69%6C%6C%27%2C%27%66%69%6C%6C%27%5D%2C%20%5C%0A%09%09%09%70%6E%6C%3A%20%50%61%6E%65%6C%20%7B%20%74%79%70%65%3A%27%74%61%62%62%65%64%70%61%6E%65%6C%27%2C%20%5C%0A%09%09%09%09%61%62%6F%75%74%54%61%62%3A%20%50%61%6E%65%6C%20%7B%20%74%79%70%65%3A%27%74%61%62%27%2C%20%74%65%78%74%3A%27%44%65%73%63%72%69%70%74%69%6F%6E%27%2C%20%5C%0A%09%09%09%09%09%61%62%6F%75%74%45%74%3A%20%45%64%69%74%54%65%78%74%20%7B%20%74%65%78%74%3A%27%22%20%2B%20%74%68%69%73%2E%6C%6F%63%28%61%62%6F%75%74%53%74%72%29%20%2B%20%22%27%2C%20%70%72%65%66%65%72%72%65%64%53%69%7A%65%3A%5B%33%36%30%2C%32%30%30%5D%2C%20%70%72%6F%70%65%72%74%69%65%73%3A%7B%6D%75%6C%74%69%6C%69%6E%65%3A%74%72%75%65%7D%20%7D%20%5C%0A%09%09%09%09%7D%2C%20%5C%0A%09%09%09%09%75%73%61%67%65%54%61%62%3A%20%50%61%6E%65%6C%20%7B%20%74%79%70%65%3A%27%74%61%62%27%2C%20%74%65%78%74%3A%27%55%73%61%67%65%27%2C%20%5C%0A%09%09%09%09%09%75%73%61%67%65%45%74%3A%20%45%64%69%74%54%65%78%74%20%7B%20%74%65%78%74%3A%27%22%20%2B%20%74%68%69%73%2E%6C%6F%63%28%75%73%61%67%65%53%74%72%29%20%2B%20%22%27%2C%20%70%72%65%66%65%72%72%65%64%53%69%7A%65%3A%5B%33%36%30%2C%32%30%30%5D%2C%20%70%72%6F%70%65%72%74%69%65%73%3A%7B%6D%75%6C%74%69%6C%69%6E%65%3A%74%72%75%65%7D%20%7D%20%5C%0A%09%09%09%09%7D%20%5C%0A%09%09%09%7D%2C%20%5C%0A%09%09%09%62%74%6E%73%3A%20%47%72%6F%75%70%20%7B%20%6F%72%69%65%6E%74%61%74%69%6F%6E%3A%27%72%6F%77%27%2C%20%61%6C%69%67%6E%6D%65%6E%74%3A%5B%27%66%69%6C%6C%27%2C%27%62%6F%74%74%6F%6D%27%5D%2C%20%5C%0A%09%09%09%09%6F%74%68%65%72%53%63%72%69%70%74%73%42%74%6E%3A%20%42%75%74%74%6F%6E%20%7B%20%74%65%78%74%3A%27%4F%74%68%65%72%20%53%63%72%69%70%74%73%2E%2E%2E%27%2C%20%61%6C%69%67%6E%6D%65%6E%74%3A%5B%27%6C%65%66%74%27%2C%27%63%65%6E%74%65%72%27%5D%20%7D%2C%20%5C%0A%09%09%09%09%6F%6B%42%74%6E%3A%20%42%75%74%74%6F%6E%20%7B%20%74%65%78%74%3A%27%4F%6B%27%2C%20%61%6C%69%67%6E%6D%65%6E%74%3A%5B%27%72%69%67%68%74%27%2C%27%63%65%6E%74%65%72%27%5D%20%7D%20%5C%0A%09%09%09%7D%20%5C%0A%09%09%7D%22%3B%20%0A%09%09%64%6C%67%2E%67%72%20%3D%20%64%6C%67%2E%61%64%64%28%72%65%73%29%3B%0A%09%09%0A%09%09%64%6C%67%2E%67%72%2E%70%6E%6C%2E%61%62%6F%75%74%54%61%62%2E%61%62%6F%75%74%45%74%2E%6F%6E%43%68%61%6E%67%65%20%3D%20%64%6C%67%2E%67%72%2E%70%6E%6C%2E%61%62%6F%75%74%54%61%62%2E%61%62%6F%75%74%45%74%2E%6F%6E%43%68%61%6E%67%69%6E%67%20%3D%20%66%75%6E%63%74%69%6F%6E%20%28%29%0A%09%09%7B%0A%09%09%09%74%68%69%73%2E%74%65%78%74%20%3D%20%75%74%69%6C%73%2E%6C%6F%63%28%61%62%6F%75%74%53%74%72%29%2E%72%65%70%6C%61%63%65%28%2F%5C%5C%72%2F%67%2C%20%27%5C%72%27%29%2E%72%65%70%6C%61%63%65%28%2F%5C%5C%27%2F%67%2C%20%22%27%22%29%3B%0A%09%09%7D%3B%0A%09%09%0A%09%09%64%6C%67%2E%67%72%2E%70%6E%6C%2E%75%73%61%67%65%54%61%62%2E%75%73%61%67%65%45%74%2E%6F%6E%43%68%61%6E%67%65%20%3D%20%64%6C%67%2E%67%72%2E%70%6E%6C%2E%75%73%61%67%65%54%61%62%2E%75%73%61%67%65%45%74%2E%6F%6E%43%68%61%6E%67%69%6E%67%20%3D%20%66%75%6E%63%74%69%6F%6E%20%28%29%0A%09%09%7B%0A%09%09%09%74%68%69%73%2E%74%65%78%74%20%3D%20%75%74%69%6C%73%2E%6C%6F%63%28%75%73%61%67%65%53%74%72%29%2E%72%65%70%6C%61%63%65%28%2F%5C%5C%72%2F%67%2C%20%27%5C%72%27%29%2E%72%65%70%6C%61%63%65%28%2F%5C%5C%27%2F%67%2C%20%22%27%22%29%3B%0A%09%09%7D%3B%0A%09%09%09%0A%09%09%64%6C%67%2E%67%72%2E%62%74%6E%73%2E%6F%74%68%65%72%53%63%72%69%70%74%73%42%74%6E%2E%6F%6E%43%6C%69%63%6B%20%3D%20%66%75%6E%63%74%69%6F%6E%20%28%29%0A%09%09%7B%0A%09%09%09%76%61%72%20%63%6D%64%20%3D%20%22%22%3B%0A%09%09%09%76%61%72%20%75%72%6C%20%3D%20%22%68%74%74%70%3A%2F%2F%61%65%73%63%72%69%70%74%73%2E%63%6F%6D%2F%61%75%74%68%6F%72%73%2F%6D%2D%70%2F%6E%61%62%2F%22%3B%0A%09%0A%09%09%09%69%66%20%28%24%2E%6F%73%2E%69%6E%64%65%78%4F%66%28%22%57%69%6E%22%29%20%21%3D%20%2D%31%29%0A%09%09%09%7B%0A%09%20%20%20%20%20%20%20%20%09%69%66%20%28%46%69%6C%65%28%22%43%3A%2F%50%72%6F%67%72%61%6D%20%46%69%6C%65%73%2F%4D%6F%7A%69%6C%6C%61%20%46%69%72%65%66%6F%78%2F%66%69%72%65%66%6F%78%2E%65%78%65%22%29%2E%65%78%69%73%74%73%29%0A%09%09%09%09%09%63%6D%64%20%2B%3D%20%22%43%3A%2F%50%72%6F%67%72%61%6D%20%46%69%6C%65%73%2F%4D%6F%7A%69%6C%6C%61%20%46%69%72%65%66%6F%78%2F%66%69%72%65%66%6F%78%2E%65%78%65%20%22%20%2B%20%75%72%6C%3B%0A%09%09%09%09%65%6C%73%65%20%69%66%20%28%46%69%6C%65%28%22%43%3A%2F%50%72%6F%67%72%61%6D%20%46%69%6C%65%73%20%28%78%38%36%29%2F%4D%6F%7A%69%6C%6C%61%20%46%69%72%65%66%6F%78%2F%66%69%72%65%66%6F%78%2E%65%78%65%22%29%2E%65%78%69%73%74%73%29%0A%09%09%09%09%09%63%6D%64%20%2B%3D%20%22%43%3A%2F%50%72%6F%67%72%61%6D%20%46%69%6C%65%73%20%28%78%38%36%29%2F%4D%6F%7A%69%6C%6C%61%20%46%69%72%65%66%6F%78%2F%66%69%72%65%66%6F%78%2E%65%78%65%20%22%20%2B%20%75%72%6C%3B%0A%09%09%09%09%65%6C%73%65%0A%09%09%09%09%09%63%6D%64%20%2B%3D%20%22%43%3A%2F%50%72%6F%67%72%61%6D%20%46%69%6C%65%73%2F%49%6E%74%65%72%6E%65%74%20%45%78%70%6C%6F%72%65%72%2F%69%65%78%70%6C%6F%72%65%2E%65%78%65%20%22%20%2B%20%75%72%6C%3B%0A%09%09%09%7D%0A%09%09%09%65%6C%73%65%0A%09%09%09%09%63%6D%64%20%2B%3D%20%22%6F%70%65%6E%20%5C%22%22%20%2B%20%75%72%6C%20%2B%20%22%5C%22%22%3B%20%20%20%20%20%20%20%20%20%0A%09%09%09%74%72%79%0A%09%09%09%7B%0A%09%09%09%09%73%79%73%74%65%6D%2E%63%61%6C%6C%53%79%73%74%65%6D%28%63%6D%64%29%3B%0A%09%09%09%7D%0A%09%09%09%63%61%74%63%68%28%65%29%0A%09%09%09%7B%0A%09%09%09%09%61%6C%65%72%74%28%65%29%3B%0A%09%09%09%7D%0A%09%09%7D%3B%0A%09%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%0A%09%09%64%6C%67%2E%67%72%2E%62%74%6E%73%2E%6F%6B%42%74%6E%2E%6F%6E%43%6C%69%63%6B%20%3D%20%66%75%6E%63%74%69%6F%6E%20%28%29%20%0A%09%09%7B%0A%09%09%09%64%6C%67%2E%63%6C%6F%73%65%28%29%3B%20%0A%09%09%7D%3B%0A%09%20%20%20%20%20%20%20%0A%09%09%64%6C%67%2E%63%65%6E%74%65%72%28%29%3B%0A%09%09%64%6C%67%2E%73%68%6F%77%28%29%3B'));
	};

	// Creates a bounding box object
	this.BoundingBox = function (width,height,xoffset,yoffset)
	{
		var bBox = new Object();
		bBox.width = width;
		bBox.height = height;
		bBox.xoffset = xoffset;
		bBox.yoffset = yoffset;
		return bBox;
	};

	// Computes the bounding box of a layer
	this.getLayerBoundingBox = function (layer)
	{
		var bBox = null;
		var comp = layer.containingComp;
		var prevSelLayers = comp.selectedLayers;
		var curTime = comp.time;

		if (layer instanceof ShapeLayer)
		{
			while (comp.selectedLayers.length) comp.selectedLayers[0].selected = false;
			layer.selected = true;
			app.executeCommand(2367); // 2367 : app.findMenuCommandId("New Mask")
			var mask = layer.Masks.property(layer.Masks.numProperties);
			var verts = mask.maskShape.value.vertices;
   			mask.remove();
   			for (var z = 0; z < prevSelLayers.length; z++)	prevSelLayers[z].selected = true;

			bBox = this.BoundingBox(Math.abs(verts[0][0] - verts[3][0]),Math.abs(verts[0][1] - verts[1][1]),verts[0][0],verts[0][1]);
		}
		else if (layer instanceof AVLayer || layer instanceof TextLayer)
		{
			var maskGrp = layer.Masks;
			if (maskGrp.numProperties)
			{
				var T = Infinity;				var B = -Infinity;				var L = Infinity;				var R = -Infinity;
				for (var m = 1; m <= maskGrp.numProperties; m++)
				{
					var mask = maskGrp.property(m);
					var maskShape = mask.maskShape;

					var shape = maskShape.valueAtTime(curTime, false);
					var verts = shape.vertices;
					var intan = shape.inTangents;
					var outtan = shape.outTangents;

					for (var i = 0; i < verts.length; i++)					{						T = this.min4(T, verts[i][1], verts[i][1]+intan[i][1], verts[i][1]+outtan[i][1]);						B = this.max4(B, verts[i][1], verts[i][1]+intan[i][1], verts[i][1]+outtan[i][1]);						L = this.min4(L, verts[i][0], verts[i][0]+intan[i][0], verts[i][0]+outtan[i][0]);						R = this.max4(R, verts[i][0], verts[i][0]+intan[i][0], verts[i][0]+outtan[i][0]);					}
			   	}
				bBox = this.BoundingBox(R-L,B-T,L,T);
			}
			else if (layer instanceof TextLayer)
			{
				while (comp.selectedLayers.length) comp.selectedLayers[0].selected = false;
				layer.selected = true;
				app.executeCommand(2367); // 2367 : app.findMenuCommandId("New Mask")
				var mask = layer.Masks.property(layer.Masks.numProperties);
				var verts = mask.maskShape.value.vertices;
	   			mask.remove();
	   			for (var z = 0; z < prevSelLayers.length; z++)	prevSelLayers[z].selected = true;

	   			bBox = this.BoundingBox(Math.abs(verts[0][0] - verts[3][0]),Math.abs(verts[0][1] - verts[1][1]),0,0);
	   			if (verts[0][0] != 0)
		   		{
		   			bBox.xoffset += verts[0][0];
					bBox.yoffset += verts[1][1];
		   		}
			}
			else
			{
				bBox = this.BoundingBox(layer.width,layer.height,0,0);
			}
		}
		return bBox;
	};

	// Computes the minimum value between four numbers
	this.min4 = function (n1, n2, n3, n4)
	{
		return (n1 < n2 && n1 < n3 && n1 < n4) ? n1 :
			  ((n2 < n1 && n2 < n3 && n2 < n4) ? n2 :
			  ((n3 < n1 && n3 < n2 && n3 < n4) ? n3 : n4));
	};

	// Computes the maximum value between four numbers
	this.max4 = function (n1, n2, n3, n4)
	{
		return (n1 > n2 && n1 > n3 && n1 > n4) ? n1 :
			  ((n2 > n1 && n2 > n3 && n2 > n4) ? n2 :
			  ((n3 > n1 && n3 > n2 && n3 > n4) ? n3 : n4));
	};
}


// run script
new RepositionAnchorPoint().run(this);
