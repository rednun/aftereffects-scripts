// Ease and Wizz 2.0.6 : Curvaceous : inCirc : all keyframes
// Ian Haigh (http://ianhaigh.com/easeandwizz/)
// Last built: 2015-03-25T17:38:30+11:00


function easeandwizz_inCirc(t, b, c, d) {
	return -c * (Math.sqrt(1 - (t/=d)*t) - 1) + b;
}


function easeAndWizz() {
	try {
		var key1 = key(1);
		var key2 = key(numKeys);
	} catch(e) {
		return null;
	}
	
	t = time - key1.time;
	d = key2.time - key1.time;

	sX = key1.time;
	eX = key2.time - key1.time;


	if ((time < key1.time) || (time > key2.time)) {
		return null;
	} else {
		return valueAtTime(easeandwizz_inCirc(t, sX, eX, d));
	}
}

(easeAndWizz() || value);

