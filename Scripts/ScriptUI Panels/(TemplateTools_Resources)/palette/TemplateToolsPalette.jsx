function TemplateToolsPalette()
{
	var TemplateTools = this;
	this.buildUI = function (thisObj)
	{
		// dockable panel or palette
		var pal = (thisObj instanceof Panel) ? thisObj : new Window("palette", globals.scriptTitle, [0,0,0,0], {resizeable:true});

		// UI code
		panel_vars = pal.add("panel",[0,0,0,0], globals.scriptTitle);
		
		// edittext_1 = panel_vars.add("edittext",[0,0,200,20], globals.variables, {readonly:0,noecho:0,borderless:0,multiline:1,enterKeySignalsOnChange:0});

		// edittext_2 = panel_vars.add("edittext",[0,0,200,20], "(paste csv)", {readonly:0,noecho:0,borderless:0,multiline:1,enterKeySignalsOnChange:0});

		but_comps = panel_vars.add("button",[0,0,200,20],"add config comps");
		but_comps.helpTip = "Create NGDATA SmartVideo Template";

		// but_inpoints = panel_vars.add("button",[0,0,200,20],"get inpoints");
		// but_inpoints.helpTip = "Get list of inpoints of selected layers";

		// but_cornerpin = panel_vars.add("button",[0,0,200,20],"export cornerpin");
		// but_cornerpin.helpTip = "Select Cornerpin to export";

		but_import_smartclips = panel_vars.add("button",[0,0,200,20],"import/update SmartClips");
		but_import_smartclips.helpTip = "Create or update SmartClip Compositions from JSON file and create SmartVideo Schematics";

		// but_update_smartclips = panel_vars.add("button",[0,0,200,20],"update SmartClips csv");
		// but_update_smartclips.helpTip = "Create or update SmartClip Compositions from .csv file";

		but_export_smartclips = panel_vars.add("button",[0,0,200,20],"export SmartClip Durations");
		but_export_smartclips.helpTip = "Export SmartClip Compositions to JSON file";

		but_update_smartclips = panel_vars.add("button",[0,0,200,20],"Update SmartClip Durations");
		but_update_smartclips.helpTip = "Update SmartClip Compositions to NEW JSON file";


		but_preflight = panel_vars.add("button",[0,0,200,20],"preflight Template");
		but_preflight.helpTip = "Reduce project for every single selected comp (and include comps starting with `!`)\rSaving to projectname with the comp name added in same folder";

		but_help = panel_vars.add("button",[0,0,200,20],"?");
		but_help.helpTip = "Help";


		image_logo = pal.add("image", [0,0,80,64], globals.imageLogo); 
		
		// event callbacks
		but_comps.onClick = function() {
			//test();
			CreateTemplate(globals.compSettings);
		}

		// but_inpoints.onClick = function() {
		// 	// alert("click!");
		// 	GetInpoints();
		// }

		// but_cornerpin.onClick = function() {
		// 	//test();
		// 	ExportCornerpin();
		// }
		but_import_smartclips.onClick = function() {
			ImportSmartClips();
		}


		but_export_smartclips.onClick = function() {
			ExportSmartClips();
		}

		but_update_smartclips.onClick = function() {
			ExportSmartClipsUpdateJson();
		}

		but_preflight.onClick = function() {
			PreFlight();
		}

		but_help.onClick = function() {
			Help();
		}

		// edittext_1.onChange = function() {
		// 	globals.variables = this.text;
		// 	if(this.text.indexOf("first_name")>=0 && this.text.indexOf("preposition")>0 && this.text.indexOf("last_name")>0) {
		// 		globals.add_fullname = 1;
		// 	} else {
		// 		globals.add_fullname = 0;
		// 	}

		// 	if(this.text.indexOf("address_1")>0 && this.text.indexOf("house_number")>0 && this.text.indexOf("suffix")>0 && this.text.indexOf("zip_code")>0 && this.text.indexOf("city")>0 && this.text.indexOf("country")>0) {
		// 		globals.add_fulladdress = 1;
		// 	} else {
		// 		globals.add_fulladdress = 0;
		// 	}
		// }

		// edittext_2.onChange = function() {
		// 	var headers = CsvHeaders(this.text);
		// 	var json = CsvToJson(this.text);
		// 	//update textfield
		// 	edittext_1.text = headers;
		// 	//set globals
		// 	globals.variables = headers;
		// 	globals.variableData = json;
		// 	globals.dummyCsv = this.text;
		// 	//try update dummyData comp
		// 	UpdateTextLayer(globals.dummyCsvLayer, this.text, 0);
		// }

		// show user interface
		if (pal instanceof Window)
		{
			pal.center();
			pal.show();
		}
		else
		{
			pal.layout.layout(true);
		}
	};

	this.run = function (thisObj)
	{
		this.buildUI(thisObj);
	};
}