// ---- expressions -----

function _exp_check_data() {
	return _get_expression("check_data_v1.6");
}


function _exp_booleanize() {
	return _get_expression("Booleanize");
}


function _exp_numberize() {
	return _get_expression("Numberize_v0.2");

}

function _exp_break_text() {
	return _get_expression("break_text");
}


function _exp_cornerpin_to_comp() {
	return _get_expression("cornerpin_to_comp_v0.1");
}

function _exp_default(txt_array) {
	return 'eval( comp("!Functions").layer("check_data").text.sourceText.valueAtTime(0) ); //this script\
res = check_data(thisLayer); //parameters: field name, default value; e.g. res = check_data(thisLayer, true);';
}

function _exp_settings() {
	return 'dtd = thisComp.layer("disable_testdata").text.sourceText.valueAtTime(0);\
res = 0;\
if (dtd.substr(0,1) == "{") {\
 idx = effect("dummydata_idx")("Slider").valueAtTime(0);\
 res = idx > 0 ? idx : value;\
}\
res;'
}

function _exp_full_name() {
	return 'fn = thisComp.layer("first_name").text.sourceText.valueAtTime(0);\
pf = thisComp.layer("preposition").text.sourceText.valueAtTime(0);\
ln = thisComp.layer("last_name").text.sourceText.valueAtTime(0);\
na = [];\
if(fn.length > 0) { na.push(fn); }\
if(pf.length > 0) { na.push(pf); }\
if(ln.length > 0) { na.push(ln); }\
na.join(" ");';
}


function _exp_full_address() {
	return 'street = thisComp.layer("address_1").text.sourceText.valueAtTime(0);\
nr = thisComp.layer("house_number").text.sourceText.valueAtTime(0);\
suf = thisComp.layer("suffix").text.sourceText.valueAtTime(0);\
zip = thisComp.layer("zip_code").text.sourceText.valueAtTime(0);\
city = thisComp.layer("city").text.sourceText.valueAtTime(0);\
country = thisComp.layer("country").text.sourceText.valueAtTime(0);\
res = street + " " + nr + " " + suf + "\\r";\
res += zip + " " + city + "\\r";\
res += country;'
}


function _exp_scale() {
	return 'tw = effect("text_width")("Slider").valueAtTime(0);\
maxw = effect("max_width")("Slider").valueAtTime(0);\
maxs = effect("max_scale")("Slider").valueAtTime(0);\
mins = effect("min_scale")("Slider").valueAtTime(0);\
s = tw > 0 ? maxw/tw : 0;\
s = Math.min(maxs, s);\
s = Math.max(mins, s);';
}



//Load external expressions

function _get_expression(scriptname) {
	var scriptPath = File($.fileName).parent.fsName;
	var scriptFile = File(scriptPath + "/Expressions/"+scriptname+".js");
	// alert("scriptPath "+scriptPath);
	if(scriptFile.exists) {
		// alert("External file [" + scriptFile.path + "/" + scriptFile.name + "] found!");
		// var res = "";
		// try {
			res = $.evalFile(scriptFile);
			// alert("blob: "+res);
			res = res.replace(/\\/g, "\\"); //escape backslashes
			res = res.replace(/\n/g, "\r"); //change return and add \ before linebreaks
		// } catch (err) { alert("error reading "+scriptFile) }
		return res;
	} else {
		alert("External file [" + scriptFile.path + "/" + scriptFile.name + "] not found!");
		return "";
	}
}

