﻿function Help(thisObj)
{
	//help texts
	var add_config_comps = """Create __Config folder with !!Settings comp""";
	var import_smartclips = """import JSON file to create or update SmartClip compositions""";
	var preflight = """save project and create Reduced Project copies for selected compositions, keeping comps starting with '!'""";


	//create window and layout	
	var dlg = new Window('dialog', 'Template Tools Help');
	//add_config_comps
	dlg.msgPnl = dlg.add('panel', undefined, 'add config comps');
	// dlg.msgPnl0.titleSt = dlg.msgPnl.add('statictext', undefined, 'add config comps');
	dlg.msgPnl.msgSt = dlg.msgPnl.add('statictext', undefined, add_config_comps, {multiline:true});

	dlg.msgPnl = dlg.add('panel', undefined, 'import/update SmartClips');
	dlg.msgPnl.msgSt = dlg.msgPnl.add('statictext', undefined, import_smartclips, {multiline:true});

	dlg.msgPnl = dlg.add('panel', undefined, 'preflight Template');
	dlg.msgPnl.msgSt = dlg.msgPnl.add('statictext', undefined, preflight, {multiline:true});

	var but_close = dlg.add('button', [0,0,200,20],"close");


	//functions
	but_close.onClick = function() {
		this.parent.close();
	}


	//show
	dlg.show();
}