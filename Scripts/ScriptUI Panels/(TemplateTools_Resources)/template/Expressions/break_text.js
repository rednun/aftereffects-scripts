"""
//THIS FILE IS ESCAPE WITH TRIPLE QUOTES SO IT CAN BE LOADED BY OTHER JS FUNCTIONS
//ALSO BACKSLASHES NEED TO BE DOUBLED

/*
	break_text.js for Adobe After Effects
	(c) guido@rednun.nl

	version 1.0 14.... - initial version
	version 1.1 150831 - Built for RTL7
	version 1.2 151102 - New version for Rednun
	version 1.3 151124 - Added jsfiddle link for testing and optimized code
	version 160519 - Recognize \\n, \\n\\r or \\r\\n as linebreak and also remove spaces around linebreaks
	version 170221 - Added min_lines option

	add line breaks to a string
		- unless there already is a linebreak ('\\r') in the string
		- looks for spaces ' ' or else '-' to break on
		- all values are in characters (not pixels!)

	usage:
		eval( comp("!Functions").layer("break_text").text.sourceText.valueAtTime(0) ); //load this script
		txt = thisLayer.text.sourceText.valueAtTime(0);
		break_text(txt, {max_lines:2, min_chars:10});

	returns:
		string with '\\r' for line breaks

	options (if value is null/0/false the default value between parentheses will be used):
		txt 		the string to be broken
		settings	named object with settings:
			min_chars		(10) text shorter the min_chars will not break
			max_lines		(-1) maximum lines: line length will be as close as possible to average length
			chars_per_line	(20) preferred amount of chars per line: if max_lines < 0 this is used to calculate number of lines
			allow_orphans 	(false) allow orphans
			orphan_length 	(3) minimum characters to consider new line to be an orphan
			override_breaks (false) if set to true ignore/remove \\r in source text

 
	Try it out and Fiddle here:
		https://jsfiddle.net/Rednun/1qhvmyzx/8/

*/

//break_text(value, {min_chars:10});

// -------- functions --------
function break_text(txt, config_settings) {
	//default settings if not specified
	var default_settings = {
		"min_chars": 10,
		"max_lines": -1,
		"chars_per_line": 20,
		"allow_orphans": false,
		"orphan_length": 3,
		"override_breaks": false
	};
	var merged_settings = merge_objects(default_settings, config_settings);
	
	//check if we already have linebreaks
	if (txt.indexOf("\\r") > 0) {
		if (merged_settings.override_breaks) {
			txt = txt.replace(/\\n\\r/g, " "); //replace linebreaks with spaces
			txt = txt.replace(/\\r\\n/g, " "); //replace linebreaks with spaces
			txt = txt.replace(/\\r/g, " "); //replace linebreaks with spaces
		} else {
			txt = txt.replace(/\\r\\n/g, "\\r");
			txt = txt.replace(/\\n\\r/g, "\\r");
			txt = txt.replace(/\\r/g, "\\r");
			return txt; //return unaltered if linebreaks are present
		}
	}

   if (txt.indexOf("\\n") > 0) {
		if (merged_settings.override_breaks) {
			txt = txt.replace(/\\n/g, " "); //replace linebreaks with spaces
		} else {
			txt = txt.replace(/\\n/g, "\\r");
			return txt; //return unaltered if linebreaks are present
		}
	}

	//check text length and calculate line_cnt and avg_line_length
	var txt_w = txt.length;
	if (txt_w <= merged_settings.min_chars) {
		return txt; //dont break text, return unaltered
	} else {
		//calculate number of lines
		if(merged_settings.chars_per_line > 0) {
			line_cnt = Math.ceil(txt_w / merged_settings.chars_per_line);
		} else {
			line_cnt = 1;   
		}
		if (merged_settings.min_lines >= 0) {
			//make sure we do not have more lines then allowed
			line_cnt = Math.max(line_cnt, merged_settings.min_lines);
		}
		if (merged_settings.max_lines > 0) {
			//make sure we do not have more lines then allowed
			line_cnt = Math.min(line_cnt, merged_settings.max_lines);
		}
		line_cnt = Math.max(1, line_cnt); //line_cnt is always 1 or bigger
		
		//average line length
		avg_line_length = Math.round(txt.length / line_cnt);
	   // return "avg: " + avg_line_length + " line_cnt:" + line_cnt + " maxlines:" + merged_settings.max_lines;
	}

	//split words and parts into an array
	broken_arr = textArr(txt);
	//return broken_arr.join("|");

	//join array into array of lines
	broken_txt_arr = join_text(broken_arr, avg_line_length, merged_settings.allow_orphans, merged_settings.max_lines, merged_settings.orphan_length);

	broken_txt = broken_txt_arr.join("\\r");

	return broken_txt;
}



function join_text(words, avg_line_length, allow_orphans, max_lines, orphan_length) {
	//inits
	var lines = []; //array to hold lines
	var line = words[0]; //add first word to line

	// start joining words and insert line breaks 	
	for (var w = 1; w < words.length; w++) {
		var word = words[w];

		//catch orphans
		var first_word = (w == 1) ? true : false;
		var last_word = (w >= words.length - 1) ? true : false;

		//if first word or last word AND word is too small OR current line is too small; then this word is an orphan
		var is_orphan = false;
		if (last_word) {
			if (word.length <= orphan_length || line.length <= orphan_length) {
				is_orphan = true;
			}
		}

		//create new line
		new_line = line; //continue with last value for line
		new_line += (new_line.substr(-1) == "-" || (is_orphan && allow_orphans)) ? "" : " "; //add space, except for dash or orphan word
		new_line += word; //add word

		//decide if to break
		if (is_orphan) {
			will_break = allow_orphans;
		} else {
			will_break = new_line.length > avg_line_length ? true : false;
		}

		//can break?
		can_break = max_lines > 0 && lines.length - 1 >= max_lines ? false : true;


		//debug lines
		//lines.push("[DEBUG "+w+" last_word:"+last_word+" is_orphan:"+is_orphan+" will_break:"+will_break+" can_break:"+can_break+" line:"+line+" ]");


		//break lines
		if (can_break && will_break) {
			//dont add new word, but start new line
			//but only if not last word while no_orphans is true, or no_orphans is false
			lines.push(line); //add new line
			line = word; //start new line with next word
		} else {
			//add new word and continue
			line = new_line;
		}


	}

	// return broken text
	if (max_lines > 0 && lines.length >= max_lines) {
		lines.push(lines.pop() + " " + line); //add last line to last item in array
	} else {
		lines.push(line);
	}
	return lines;
}


function textArr(txt) {
	broken_arr = [];
	// try to break text on space
	txt_arr = txt.split(" ");

	// break each word into parts
	for (i = 0; i < txt_arr.length; i++) {
		word = txt_arr[i];
		wordparts_arr = word.split("-");
		// put everything in an array
		if (wordparts_arr.length > 1) {
			for (j = 0; j < wordparts_arr.length; j++) {
				newpart = wordparts_arr[j]
				newpart += j < wordparts_arr.length - 1 ? "-" : "";
				broken_arr.push(newpart);
			}
		} else {
			broken_arr.push(word);
		}
	}
	return broken_arr;
}



function merge_objects(obj1, obj2) {
	var obj3 = {};
	for (var attrname in obj1) {
		obj3[attrname] = obj1[attrname];
	}
	for (var attrname in obj2) {
		obj3[attrname] = obj2[attrname];
	}
	return obj3;
}

"""