"""
//THIS FILE IS ESCAPE WITH TRIPLE QUOTES SO IT CAN BE LOADED BY OTHER JS FUNCTIONS
//ALSO BACKSLASHES NEED TO BE DOUBLED

/*
	add expression to an empty Corner Pin effect

	15011 v0.1 initial version for RT demo

	combines all transformation data (position, scale, rotation) in plain corner pins
	after setting data you can convert the expression to keyframe data to copy paste to an external text document
	or run the export_trackers_#.jsx script to create JSON files for the Realtime flow

	cornerpin_to_comp(lyr, property_name);

	properties
		lyr 			ref to other layer
		property_name	the name of the corner pin effect, usually "Corner Pin"


	example
		eval( comp("!Functions").layer("cornerpin_to_comp").text.sourceText.value ); //import function cornerpin_to_comp(lyr, property_name)
		lyr = effect("Layer Control")("Layer"); //ref to another layer
		res = cornerpin_to_comp(lyr, thisProperty.name);
		res;

*/

function cornerpin_to_comp(lyr, property_name) {
	var src_pin = lyr.effect("Corner Pin")(property_name).value;
	var tgt_pin = deangle(lyr, src_pin);
	return tgt_pin;
}

function deangle(lyr, pin_point, offset) {
	/* apply position, rotation and scale transformation to a corner pin point
		calculate rotated points
		translate layer to 0,0
			scale layer
		translate back
	*/
	//init
	var new_point = pin_point;

	//check if visible
	if(!lyr.active) {
		return [0,0];
	}

	//correct rotation
	var angle = lyr.transform.rotation;
	var angle_center = lyr.transform.anchorPoint;
	new_point = rotatePoint(pin_point, angle_center, angle);
	//reposition
	new_point -= lyr.transform.anchorPoint; //zero in on null of layer

	//correct scale 
	var lyr_scale = lyr.transform.scale/100; //layer scale
	new_point = [new_point[0]*lyr_scale[0], new_point[1]*lyr_scale[1]];
	//add position
	new_point += lyr.transform.position;

	//optional offset
	//new_point += offset;

	//optional scaling
	//new_point = scalePoint(new_point, angle_center, scaling/100)

	//return
	return new_point;
}


function scalePoint(point, origin, scale) {
	x = (point[0]-origin[0])*scale[0];
	y = (point[1]-origin[1])*scale[1];
	return [x, y];
}


function rotatePoint(point, origin, angle, scaling) {
	angle = angle * Math.PI / 180.0;
	if (typeof(scaling) == "undefined") {
		scaling = [1, 1];
	} else {
		scaling /= 100;
	}
	
	x = origin[0];
	x += scaling[0] * Math.cos(angle) * (point[0]-origin[0]);
	x -= scaling[0] * Math.sin(angle) * (point[1]-origin[1]);

	y = origin[1];
	y += scaling[1] * Math.sin(angle) * (point[0]-origin[0]);
	y += scaling[1] * Math.cos(angle) * (point[1]-origin[1]);

	return [x, y];
}
"""