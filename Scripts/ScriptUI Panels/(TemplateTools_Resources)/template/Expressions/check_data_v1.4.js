"""
//THIS FILE IS ESCAPE WITH TRIPLE QUOTES SO IT CAN BE LOADED BY OTHER JS FUNCTIONS
//ALSO BACKSLASHES NEED TO BE DOUBLED

/*
	check_data for Adobe After Effects
	(c) guido@rednun.nl

	version 1.1 151001 - Implemented in RTL4 template
	version 1.2 151109 - Use default value if field not in Dummydata
	version 1.3 151124 - Fixed default_values other types then string (e.g. number or boolean)
	version 1.4 151125 - Also use default if dummy data is empty

	check values of text field and replace it with dummy data from csv file.
	default_value is used if there is nu dummy or database data.

	Name the text layer to the database field to map to
	The contents of the text should be the name in curly quotes, e.g. {txt_field}

	usage:
		res = check_data(thisLayer, default_value);

	parameters:
		thisLayer (object)
			current layer to derive name from and lookup in csv data
		default_value (empty, string, boolean or number)
			the default value to use if no data found
*/


/* 	Sample usage:
eval( comp("!Functions").layer("check_data").text.sourceText.value ); //this script
res = check_data(thisLayer, true); //get results or returns 1 by default in this example
*/



// -- functions --
function check_data(this_layer, default_value) {
	default_value = typeof default_value === "undefined" ? "" : String(default_value);
	//below the code to evaluate
	var testdata_idx = Number(thisComp.layer("settings").text.sourceText.value);
	//get dummy/test data
	var testdata_arr = [default_value, this_layer.name, this_layer.text.sourceText.value];
	//add csv data to array
	var csv = comp("!DummyData").layer("csv_data").text.sourceText.value;
	var list = CsvToArray(csv, this_layer.name);
	testdata_arr = testdata_arr.concat(list);
	//decide which data to show
	res = this_layer.text.sourceText.value;
	if(res.substr(0,1) == "{") {
		testdata_idx = Math.min(testdata_idx, testdata_arr.length-1);
		res = testdata_idx > 0 && testdata_arr[testdata_idx] != "" ? testdata_arr[testdata_idx] : default_value; //use testdata or default value
		//res = res.length > 0 && res.substr(0,1) != "{" ? res : default_value; //use default value if still empty
	}
	//process data
	if(typeof res === "string" && res != "") {
		//detect linebreaks
		res = res.replace(/\\\\r/gi, '\\r');
		//time format
		res = this_layer.name.substr(0,8) == "txt_tijd" ? cleanTime(res) : res;
		//timecode format
		res = this_layer.name.substr(0,3) == "tc_" ? cleanTimeCode(res) : res;
	}
	//if name starts with 'toggle' force boolean output
	var bool = Boolean(Number(res)) ? 1 : 0;
	res = this_layer.name.substr(0,7) == "toggle_" ? bool : res;

	//return
	return res;
}

function CsvToArray(csvdata, field) {
	var delimeter = ";";
	//get header
	var csv_arr = csvdata.match(/[^\\r\\n]+/g);
	var header_arr = csv_arr.shift().split(delimeter);
	var data_obj = new Object();
	data_arr = new Array();
	//add row data to data object
	for(var i=0;i<csv_arr.length;i++) {
		var row_data = csv_arr[i].split(delimeter);
		for(var j=0;j<row_data.length;j++) {
			var field_name = header_arr[j];
			if(field_name == field) {
				var obj_value = row_data[j];
				data_arr.push(obj_value);
			}
		}
	}
	//return data object
	return data_arr;
}

function cleanTime(str) {
	var res = str.match(/^\\s*\\d{2}.\\d{2}\\s*$/); //check if string could be a time notation, e.g. " 12'34  "
	if(res) {
		res = str.replace(/\\s*/g, ""); //remove all whitespaces
		res = res.replace(/\\D/g, "."); //replace all non digits with '.'
		return res;
	} else {
		return str; //else return original string
	}
}


function cleanTimeCode(str) {
	var fps = 25;
	var res = -1;
	if(str.match(/'/)) {
		var s = str.split("'")[0];
		var f = str.split("'")[1];
		var time_str = s + "." + f*100/fps;
		res = Number(time_str);
	} else {
		res = Number(str);
	}
	res = String(res) == "NaN" ? -1 : res;
	return res;
}
"""