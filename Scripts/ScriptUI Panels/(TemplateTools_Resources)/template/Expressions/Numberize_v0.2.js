"""
//THIS FILE IS ESCAPE WITH TRIPLE QUOTES SO IT CAN BE LOADED BY OTHER JS FUNCTIONS
//ALSO BACKSLASHES NEED TO BE DOUBLED

/*
	Numberize.js
	20151027 v0.1 Safe converting of a string to a number; result is 0 on error/exception
	20151125 v0.2 Improved handling of non numbers

	usage:
	eval(comp("!Functions").layer("Numberize").text.sourceText.value); //import Numberize()
	var str = "1";
	var nr = Numberize(str); //1
*/

function Numberize(str) {
	var n = 0;
	try { 
		if(str == "") throw "empty";
		if(isNaN(str)) throw "not a number";
		n = Number(str);
	}
	catch(err) {
		//throw "[Error parsing ["+str+"] -["+err+"]";
		n = 0;
	}
	return n;
}
"""