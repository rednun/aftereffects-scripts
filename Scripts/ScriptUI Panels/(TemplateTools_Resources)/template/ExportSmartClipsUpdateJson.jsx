﻿/* 
	170508 initial version

	Export new SmartVideo Config JSON from existing JSON
	updating duration field
*/


function ExportSmartClipsUpdateJson()
{

	// Prompt user to select text file
	var myFile = _openJSONFile();
	if(myFile) {
		var json = _readJSON(myFile);
	}

	//file to write to
	var folder = new Folder("~/Desktop/");
	if(app.project.file) {
		folder = app.project.file.parent;
	}
	var selectedFolder = folder.selectDlg("Select folder to save to");
	var file_path = selectedFolder.toString() + "/smartclips_export.json";
 
	//update json
	_updateJSON(json);


	//write output json to file
	if(json) {
		_writeJsonFile(file_path, json);
	} else {
		alert("json is empty "+json);
	}
}



function _updateJSON(json) {
	//for every comp in original json
	var not_found = [];
	for (var i = 0; i < json.scenes.length; i++) {
		var scene = json.scenes[i];
		for (var j = 0; j < scene.smartclips.length; j++) {
			var smartclip = scene.smartclips[j];
			for (var k = 0; k < smartclip.translations.length; k++) {
				var translation = smartclip.translations[k];
				var comp_name = smartclip['id'] + "_" + translation['language'];
				// var folder_name = clip['name'] + " (" + clip_type.substr(0,1).toUpperCase() + ")";
				// var parentfolder_name = ('00'+Math.floor(scene_order)).substr(-2) + " " + scene_name;

				var comps = _findComps(comp_name);
				if(comps.length > 1) {
					alert("composition ["+comp_name+"] found ["+comps.length+"] times!");
				} else if(comps.length < 1) {
					not_found.push(comp_name);
				} else {
					//update json
					translation.duration = comps[0].duration;

				}
			}
		}
	}
	if(not_found.length>0) {
		alert("Some compositions where not found\n"+not_found.join("\n"));
	}
}


function _writeJsonFile(file_path, data)
{
	var write_file = File(file_path);
	//
	if (!write_file.exists) {
		// if the file does not exist create one
		write_file = new File(file_path);
	} else {
		// if it exists ask the user if it should be overwritten
		var res = confirm("The file already exists.\nOverwrite "+file_path+"?", true, "titleWINonly");
		// if the user hits no stop the script
		if (res !== true) {
			return;
		}
	}

	if (write_file !== '') {
		//Open the file for writing.
		out = write_file.open('w', undefined, undefined);
		write_file.encoding = "UTF-8";
		write_file.lineFeed = "Unix"; //convert to UNIX lineFeed
		// txtFile.lineFeed = "Windows";
		// txtFile.lineFeed = "Macintosh";
	}

	// got an output?
	if (out !== false) {
		// loop the list and write each item to the file
		//for (var i = 0; i < data.length; i++) {
			var str = JSON.stringify(data, null, 2);
			write_file.write(str);
		//}
		// always close files!
		write_file.close();
	}
}
