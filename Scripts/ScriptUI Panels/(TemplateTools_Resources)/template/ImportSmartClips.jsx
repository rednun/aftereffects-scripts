﻿/* 
	170420 initial version
	170424 new _addFolder function: finds folder or creates, allows a list of foldernames; e.g. _addFolder("E2", ["A2", "B2", "C2", "D2"]);
	170502 import json smartclips
*/


function ImportSmartClips()
{
	// alert('check');

	//start undo group
	app.beginUndoGroup("Import SmartClips");

	// Prompt user to select text file
	var myFile = _openJSONFile();
	if(myFile) {
		var json = _readJSON(myFile);
		_processJSON(json);
		_createSchematic(json);
	}

	//end undogroup
	app.endUndoGroup();
}


function _openJSONFile()
{
	var myFile = File.openDialog("Please select JSON StoryLine Configuration file");
	if (myFile == null) {
		return false;
	} else {
		var fileOK = myFile.open("r");
		if (!fileOK) {
			alert("File open failed!");
			return false;
		} else {
			return myFile;
		}
	}
}

//needs/uses functiosn from UpdateSmartClips

function _readJSON(file)
{
	// Create new project
	// var currentProject = (app.project) ? app.project : app.newProject();
	var data="";
	if(file != null && file.exists) {
		file.open("r");
		data = file.read();
		file.close();
	}
	// var strJSON = '{"result":true,"count":1}';
	var json = eval("(function(){return " + data + ";})()");
	return json;
}

function _createSchematic(json) {

	//create SmartVideoSchematic from first language of each SmartClip
	var schematic = _addJSONComp(null, 'SmartVideoSchematic', 180, 960, 540, 9);
	var total_duration = 0;

	//go through scenes
	for (var i = 0; i < json.scenes.length; i++) {
		var scene = json.scenes[i];
		var longest_clip_duration = 0;
		//go through smartclips
		for (var j = 0; j < scene.smartclips.length; j++) {
			var smartclip = scene.smartclips[j];
			//go through all translations
			for (var k = 0; k < smartclip.translations.length; k++) {
				var translation = smartclip.translations[k];
				//add only first language
				if(k==0) {
					longest_clip_duration = Math.max(translation.duration, longest_clip_duration);
					var comp_name = smartclip.id + "_" + translation.language;
					//find composition
					var comps = _findComps(comp_name);
					//place first found comp and only first language
					if(comps.length > 0) {
						var lyr = schematic.layers.add(comps[0]);
						lyr.startTime = total_duration;
					}
				}
			}
		}
		total_duration += longest_clip_duration;
	}
	//set duration
	schematic.duration = total_duration;
}


function _processJSON(json) {

	//add comp
	for (var i = 0; i < json.scenes.length; i++) {
		var scene = json.scenes[i];
		for (var j = 0; j < scene.smartclips.length; j++) {
			var smartclip = scene.smartclips[j];
			for (var k = 0; k < smartclip.translations.length; k++) {

				//add comp (and audio)
				var translation = smartclip.translations[k];
				// var comment = "language == "+translation.language+" && "+smartclip.expr;
				var comment = null;
				var comp = _addJSONSmartClip(scene.order, scene.id, scene.name, translation.language, smartclip.id, smartclip.name, smartclip.type, translation.duration, scene.description, smartclip.description, comment);

				//add _info_ layer
				var txt = "SCENE INFO\r" + _stringify(scene);
				txt += "\rSMARTCLIP INFO\r" + _stringify(smartclip);
				txt += "\rTRANSLATION\r" + _stringify(translation);
				var txt_lyr = _updateJSONTextLayer(comp, "_info_", txt, null, [0,0.8,0.9]);
				txt_lyr.transform.position.setValue([10,30]);


			}
		}
	}
}

function _stringify(data)
{
	var string = "";
	// for (var i = 0; i < json.length; i++) {
	for(var key in data) {
		if(typeof(data[key]) != 'object') {
			string += key + ": " + (data[key] || "") + "\r";
		} else {
			//TODO: go deeper
		}
	}
	// alert("_stringify\n["+string+"]");
	return string;
}


function _updateJSONTextLayer(comp, name, text, comment, color)
{
	//search if exists
	var lyr = comp.layers.byName(name);
	if(lyr) {
		//update
		lyr.sourceText.setValue(text);
		lyr.comment = comment || "";
		return lyr;
	}
	//add if not found
	var txt_lyr = comp.layers.addText("x");
	txt_lyr.name = name;
	// txt_lyr.comment = comment || "";
	txt_lyr.comment = txt_lyr.Text;
	txt_lyr.label = 0;
	txt_lyr.guideLayer = true;
	txt_lyr.enabled = false; //close eye
	txt_lyr.shy = true;
	// txt_lyr.transform.position.setValue([10,30]);
	//get source text object
	var txt_prop = txt_lyr.property("Source Text");
	var txt_doc = txt_prop.value;
	// txt_doc.resetParagraphStyle();
	txt_doc.resetCharStyle();
	txt_doc.fontSize = 24;
	txt_doc.fillColor = color || [0.8,0.8,0.8];
	txt_doc.font = "Arial";
	txt_doc.applyStroke = false;
	txt_doc.applyFill = true;
	txt_doc.justification = ParagraphJustification.LEFT_JUSTIFY;
	txt_doc.text = text;
	//store back
	txt_prop.setValue(txt_doc);
	//return
	return txt_lyr
}




function _addJSONSmartClip(scene_order, scene_id, scene_name, language_code, clip_id, clip_name, clip_type, clip_duration, parentfolder_comment, folder_comment, clip_comment)
{
	//folder names
	// var comp_name = scene_id + clip_id + "_" + language_code;
	var comp_name = clip_id + "_" + language_code;
	var folder_name = clip_name + " (" + clip_type.substr(0,1).toUpperCase() + ")";
	var parentfolder_name = ('00'+Math.floor(scene_order)).substr(-2) + " " + scene_name;

	//add SmartClip Composition and folder structure
	var folder = _addFolderStructure(["_Scenes", parentfolder_name, folder_name]);
	var comp = _addJSONComp(folder, comp_name, clip_duration, 960, 540, clip_type, clip_comment);
	folder.label = (clip_type == "unique") ? 13 : ( (clip_type == "closed") ? 10 : 8 ) || 0;
	folder.comment = folder_comment;
	folder.parentFolder.comment = parentfolder_comment || "";

	//add audio folder and placeholder wav
	var audio_folder = _addFolderStructure(["_Audio", parentfolder_name, folder_name]);
	var audio = _addAudio(audio_folder, comp_name + ".wav", clip_duration, clip_type, clip_comment);
	audio_folder.label = (clip_type == "unique") ? 13 : ( (clip_type == "closed") ? 10 : 8 ) || 0
	audio_folder.comment = folder_comment;
	audio_folder.parentFolder.comment = parentfolder_comment || "";

	//place audio in comp
	var lyr = _addOnceToComp(comp, audio);
	lyr.enabled = false; //close eye of audio

	//return
	return comp;
}

function _addOnceToComp(comp, add_item)
{
	//first search for duplicate
	for (var i = 0; i < comp.layers.length; i++) {
		var lyr = comp.layers[i+1];
		if(lyr.source && lyr.source.id == add_item.id) {
			//found
			return lyr;
		}
	}
	//else add item
	var layer = comp.layers.add(add_item);
	layer.moveToEnd();
	return layer;
}


function _addFolderStructure(folderstructure)
{
	//add folder structure
	/*
		create structure: a/b/c/d
		a = find a in root || add a
		b = find b in a || add b && set b.parentFolder = a
		c = find c in b || add c && set c.parentFolder = b
		d = find d in c || add d && set d.parentFolder = c
		return d;
	*/
	var folder_name = "";
	var parent_folder = app.project.rootFolder; //start with root
	for (var i = 0; i < folderstructure.length; i++) {
		folder_name = folderstructure[i];
		// parent_name = i == 0 ? 'Root' : folderstructure[i-1];
		//find folder in parent folder
		//or add folder to parent folder
		var folder = _findFolderNameWithParent(folder_name, parent_folder) || _addFolder(folder_name, parent_folder);
		//store for next iteration
		parent_folder = folder;
	}
	//return last found or added folder
	return folder;
}


function _findFolderNameWithParent(folder_name, parent_folder)
{
	//find all matching folders
	var found_folders = _searchFolderName(folder_name);
	for (var i = 0; i < found_folders.length; i++) {
		var folder = found_folders[i];
		if(folder.parentFolder === parent_folder) {
			//found!
			return folder;
		}
	}
	//not found
	return false;
}


function _addFolder(folder_name, parent_folder)
{
	//create new folder
	var folder = app.project.items.addFolder(folder_name);
	//add to parent folder
	if(parent_folder) {
		folder.parentFolder = parent_folder;
	}
	//format folder
	folder.label = 0;
	//return
	return folder;
}

function _searchFolderName(folder_name)
{
	//return array of found folder objects
	var found_folders = [];
	for(var i = 1; i <= app.project.numItems; i++) {
		if(app.project.item(i) instanceof FolderItem && app.project.item(i).name == folder_name) {
			found_folders.push(app.project.item(i));
		}
	}
	return found_folders;
}


function _findComps(comp_name)
{
	//return array of found composition objects
	var found_comps = [];
	for(var i = 1; i <= app.project.numItems; i++) {
		if(app.project.item(i) instanceof CompItem && app.project.item(i).name == comp_name) {
			found_comps.push(app.project.item(i));
		}
	}
	return found_comps;
}



function _addAudio(parent_folder, audio_name, duration, type, comment)
{
	//find existing audio

	//search if not exist yet
	for(var i = 1; i <= app.project.numItems; i++) {
		if(app.project.item(i).typeName == "Footage" && app.project.item(i).name == audio_name) {
			asset = app.project.item(i);
			asset.parentFolder = parent_folder;
			asset.label = (type == "unique") ? 13 : ( (type == "closed") ? 10 : 8 );
			if(comment) { asset.comment = comment; } //do not clear if not set
			// asset.comment = comment || ""; //clear if not set
			return asset;
		}
	}
	//create audio placeholder
	asset = app.project.importPlaceholder(audio_name,4,4,25,duration);
	asset.parentFolder = parent_folder;
	asset.bgColor = [80/255,80/255,80/255];
	if(comment) { asset.comment = comment; }
	asset.label = 2; //yellow
	return asset;
}


function _addJSONComp(parent_folder, comp_name, duration, w, h, type, comment)
{
	var comp;
	//search if not exist yet
	for(var i = 1; i <= app.project.numItems; i++) {
		if(app.project.item(i) instanceof CompItem && app.project.item(i).name == comp_name) {
			comp = app.project.item(i);
			if(parent_folder && parent_folder.name != 'Root') { comp.parentFolder = parent_folder; }
			comp.label = (type == "unique") ? 13 : ( (type == "closed") ? 10 : 8 );
			if(comment) { comp.comment = comment; }
			return comp;
		}
	}
	//not found, add comp
	comp = app.project.items.addComp(comp_name,w,h,1,duration,25);
	//format comp
	if(parent_folder && parent_folder.name != 'Root') { comp.parentFolder = parent_folder; }
	comp.bgColor = [80/255,80/255,80/255];
	if(comment) { comp.comment = comment; }
	comp.label = (type == "unique") ? 13 : ( (type == "closed") ? 10 : ((type == "generic") ? 8 : type ) );
	return comp;
}




/* 

notes

logging:
https://forums.adobe.com/thread/1879511
http://burnignorance.com/flex-air-tips/logging-on-javascript-console-in-extendscript/

*/
