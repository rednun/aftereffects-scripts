function CsvToJson(csvdata) {
	var delimeter = ";";

	//get header
	var csv_arr = csvdata.match(/[^\r\n]+/g);
	var header_arr = csv_arr.shift().split(delimeter);

	var data_obj = new Object();
	//init data object with empty arrays
	for(var f=0;f<header_arr.length;f++) {
		var field_name = header_arr[f];
		data_obj[field_name] = new Array();
	}

	//add row data to data object
	for(var i=0;i<csv_arr.length;i++) {
		var row_data = csv_arr[i].split(delimeter);
		for(var j=0;j<row_data.length;j++) {
			var field_name = header_arr[j];
			var obj_value = row_data[j];
			data_obj[field_name].push(obj_value);
		}
	}

	//return data object
	return data_obj;
}