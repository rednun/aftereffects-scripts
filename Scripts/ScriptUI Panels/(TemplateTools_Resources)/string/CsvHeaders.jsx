function CsvHeaders(csvdata) {
//	csv = "first_name,last_name,Product\njasper,jager,Thomson\nthijs,albers,FirstChoice"
	var delimeter = ";";

	//get header
	var csv_arr = csvdata.match(/[^\r\n]+/g);
	var header_arr = csv_arr.shift().split(delimeter);

	//return
	return header_arr.join(",");
}