//remove whitespaces around a string
function chomp(raw_text)
{
	var ret = raw_text.replace(/( |\n|\r)+$/, '');
	ret = raw_text.replace(/^( |\n|\r)+/, '');
	return ret;
}